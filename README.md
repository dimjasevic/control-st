# Introduction

This is an Agda library that is a port of the
[Control.ST](http://docs.idris-lang.org/en/latest/st/state.html) Idris
library. This library allows for manipulating the state and state
machines in types. The Idris library's source code can be found in the
[Idris source code repository][idris-st-code].


[idris-st-code]: https://github.com/idris-lang/Idris-dev/blob/master/libs/contrib/Control/ST.idr
