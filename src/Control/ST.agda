module Control.ST where

open import Data.List using (List; _++_; _∷_; [])
open import Data.List.Relation.Unary.Any using (Any; here; there)
open import Level using (Level; _⊔_; zero; suc)
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_)
open import Relation.Unary using (Pred)


infix 6 _:::_

-- A resource is a pair of a label and the current type stored there
data Resource : Set₁ where
  MkRes : ∀{label s : Set} → label → s → Resource

data Var : Set where -- Phantom, just for labelling purposes
  MkVar : Var

_:::_ : {A : Set} → Var → A → Resource
_:::_ = MkRes


-- For now avoiding to reimplement what is already in the Agda
-- standard library

Resources : Set₁
Resources = List Resource


res₁ : Resources
res₁ = []

res₂ : Resources
res₂ = MkVar ::: 0 ∷ MkVar ::: 5 ∷ res₁

-- A value of type InState x ty res means that the reference x must
-- have type ty in the list of resources res.
InState : ∀{A : Set} → Var → A → Resources → Pred Resources (suc zero)
InState x ty res = Any (x ::: ty ≡_)


-- Update an entry in a context with a new state
-- updateRes : ∀{lbl : Var} → ∀{st : Set} → (res : Resources) → InState lbl st res → Set → Resources
-- updateRes = ?
